# Jeu de pendu en PHP

Par Philippe Pary, 2016 publié sous licence WTF PL, voir fichier LICENCE.txt

Cours dans le cadre de Pop School Valenciennes

## Installation

* Créez une base de données, utilisez le fichier penduApp.sql pour créer la structure des tables
* Copiez et adaptez les valeurs de config.sample.php dans un fichier config.php

## Utilisation

index.php est l’accès au jeu à proprement parler, admin.php est l’accès à l’interface d’administration

## Misc

Merci, des bisous
